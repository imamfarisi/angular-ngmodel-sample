import { Component, OnInit } from '@angular/core';
import { Users } from './models/users';
import { UserType } from './models/users-type';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {

  user = new Users()
  userType: UserType[] = []
  saveLabel = "SAVE"

  ngOnInit(): void {
    this.userType = [
      { id: 1, name: 'Tipe 1' },
      { id: 2, name: 'Tipe 2' },
      { id: 3, name: 'Tipe 3' }
    ]

    //uncomment this for set sample 
    //this.user = { "username": "imam farisi", "status": true, "type": 1 }
  }

}
